document.getElementById('sub').addEventListener("click", function() {
    localStorage.setItem('nom', document.getElementById('nom').value);
    localStorage.setItem('couleur', document.getElementById('couleur').value);
});


function restaurerLesOptions()//resélectionner les options déja choisies
{
    document.getElementById('nom').value = localStorage.getItem('nom');//remplissage du champs de nom

    var couleur = localStorage.getItem('couleur');//sélection de la liste déroulante de couleur (un peu plus dur)

    if (!couleur){return; }

    var choix = document.getElementById('couleur').getElementsByTagName('option');

    for (var i = 0; i < choix.length; i++)
    {
        if (choix[i].value == couleur)
        {
            choix[i].selected = "true";
            break;
        }
    }
}

restaurerLesOptions();
function saluer()//gestion de la boite de dialogue
{
    var ajouterNom= (localStorage['nom'])? ', ' + localStorage['nom'] : '';
    if (!localStorage['compte'])//le compte n'existe pas (première utilisation ou réinstallation)
    {
        localStorage['compte']=1;
        alert('Zozor te salue pour la première fois'+ajouterNom);
    }
    else //le compte existe
    {
        localStorage['compte']=parseInt(localStorage['compte'])+1;//on transforme la chaine de caractère en int, on incrémente, puis on stocke le nouveau nombre
        alert('Zozor te salue une '+localStorage['compte']+'eme fois'+ajouterNom);
    }
}

document.getElementsByTagName('body')[0].style.backgroundColor=localStorage['couleur'];
saluer();